<div align="center">
  <img src="https://gitlab.com/Diviercadavid/profile-repository/-/raw/main/rcs/img-prof.png" alt="Markdownify" width="150" height="190">
</div>

# Hi there! 👋 I'm Divier Cadavid, iOS Developer 🚀

Welcome to my  profile! I'm passionate about creating innovative and user-friendly mobile applications. I thrive on challenges and enjoy learning about the latest trends in technology.

## Skills ⚡️
- **Languages:** Swift, Java, C#, Objective-C ☕ ☕️  ️️
- **Tools & Frameworks:** Xcode, Android Studio, Postman, Proxyman 🛠️
- **APIs:** Google Maps, Google Places, Google Auth, Google Analytics 
- **Database:** Core Data, Firebase (Crashlytics, Notifications)
- **UI/UX:** SwiftUI, Combine with SwiftUI
- **Other:** GPS, RESTful APIs

<div align="center">
    <img src="https://gitlab.com/Diviercadavid/profile-repository/-/raw/main/rcs/ezgif-4-89c99d2175.gif" alt="Profile Image" width="550" height="200">
</div>

### Personal Projects 📱
- **CryptoSwiftUIAPP:** This app manage our portfolio and shows all the current counts on the market. It     uses MVVM Architecture, Combine, and CoreData [Link Project](https://gitlab.com/Diviercadavid/cryptoswiftuiapp) 🔥

<div align="center">
    <img src="https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/portfolio%20workflow.gif?ref_type=heads" alt="Profile Image" width="120" height="250">
    <img src="https://gitlab.com/Diviercadavid/cryptoswiftuiapp/-/raw/main/imgs/chart%20detail.gif?ref_type=heads" alt="Profile Image" width="120" height="250">
</div>

- **EarthQuakes App:** EarthQuakes app was made completely with SwiftUI and Swift 5, Architecture pattern implemented was MVVM and some design pattern such as Observer and Singleton. Data is saved in Core Data and services are working with URLSession, Combine and Maps. [Link Project](https://gitlab.com/Diviercadavid/earthquakesapp) ✌🏻
- 
<div align="center">
    <img src="https://gitlab.com/Diviercadavid/earthquakesapp/-/raw/main/src/snapshot.gif?ref_type=heads" alt="Profile Image" height="250">
    <img src="https://gitlab.com/Diviercadavid/earthquakesapp/-/raw/main/src/snapshot1.gif?ref_type=heads" alt="Profile Image" height="250">
</div>

## Additional Skills 🎖️
- Proactive learner, dedicated to staying updated with the latest trends and advancements in mobile app     development. 📚
- Strong problem-solving skills and ability to work well under pressure, delivering high-quality         solutions on time. 💪
- Excellent communication and collaboration abilities, fostering productive teamwork and positive         working relationships. 🤝

## Let's Connect!
I'm open to collaborations, discussions, and new opportunities. Feel free to connect with me on [LinkedIn](https://www.linkedin.com/in/diviercadavid/). Let's create amazing things together!

Looking forward to connecting with fellow developers and tech enthusiasts! 😊
